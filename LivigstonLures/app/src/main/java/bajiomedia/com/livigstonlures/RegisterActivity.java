package bajiomedia.com.livigstonlures;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.regex.Pattern;

import bajiomedia.com.livigstonlures.request.RegisterRequest;
import bajiomedia.com.livigstonlures.response.RegisterResponse;
import bajiomedia.com.livigstonlures.utils.FontManager;

/**
 * Created by Raúl on 07/02/2016.
 */
public class RegisterActivity extends BaseActivity {


    private EditText firstNameInput,lastNameInput,emailInput,passwordInput,zipInput;
    private CheckBox termsCheckbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        prepareToolbar();
        prepareScreen();
    }

    private void prepareScreen(){
        termsCheckbox = (CheckBox)findViewById(R.id.termsCheckbox);
        firstNameInput = (EditText)findViewById(R.id.firstNameInput);
        lastNameInput = (EditText)findViewById(R.id.lastNameInput);
        emailInput = (EditText)findViewById(R.id.emailInput);
        passwordInput = (EditText)findViewById(R.id.passwordInput);
        zipInput = (EditText)findViewById(R.id.zipInput);

        Typeface tf = FontManager.getInstance().getFont("fonts/Roboto-Regular.ttf");
        termsCheckbox.setTypeface(tf);
        termsCheckbox.setText(Html.fromHtml(getString(R.string.label_terms_conditions)));
        termsCheckbox.setMovementMethod(LinkMovementMethod.getInstance());
        firstNameInput.setTypeface(tf);
        lastNameInput.setTypeface(tf);
        emailInput.setTypeface(tf);
        passwordInput.setTypeface(tf);
        zipInput.setTypeface(tf);

    }

    public void onLoginClick(View pTarget){
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
        finish();

    }

    public void onRegisterClick(View pTarget){
        String firstName, lastName, email, password, zip;
        firstName = firstNameInput.getText().toString().trim();
        lastName = lastNameInput.getText().toString().trim();
        email = emailInput.getText().toString().trim();
        password = passwordInput.getText().toString().trim();
        zip = zipInput.getText().toString().trim();

        if(!StringUtils.isAnyEmpty(firstName, lastName, email, password, zip))
        {
            if(!isEmailValid(email)) {
                Toast.makeText(RegisterActivity.this, "Email Invalid", Toast.LENGTH_SHORT).show();
                return;
            }
            if(!isPasswordValid(password)) {
                Toast.makeText(RegisterActivity.this, "Password Invalid", Toast.LENGTH_SHORT).show();
                return;
            }

            if(!isZipValid(zip)) {
                Toast.makeText(RegisterActivity.this, "Zip Invalid", Toast.LENGTH_SHORT).show();
                return;
            }

            if(!termsCheckbox.isChecked()) {
                Toast.makeText(RegisterActivity.this, "Please Accept Terms and conditions", Toast.LENGTH_SHORT).show();
                return;
            }

            try {
                password = AeSimpleSHA1.SHA1(PWD_SALT + password);
                JSONObject params = new JSONObject();
                params.accumulate("op","newUser");
                params.accumulate("password",password);
                params.accumulate("firstname",firstName);
                params.accumulate("lastname",lastName);
                params.accumulate("email",email);
                params.accumulate("zip",zip);
                params.accumulate("language", Locale.getDefault().toString());
                doRegister(params);
            }catch(Exception e){
                Toast.makeText(RegisterActivity.this, "Failed to encrypt Password", Toast.LENGTH_SHORT).show();
            }


        }else{
            Toast.makeText(RegisterActivity.this, "Please fill all the data", Toast.LENGTH_SHORT).show();
        }


    }

    private void doRegister(JSONObject paramObject){
        RegisterRequest request = new RegisterRequest();
        request.setParameters(paramObject);

        setProgressDialog(ProgressDialog.show(this, null, "Registering User...", true));
        request.setHandler(new RegisterRequest.ResponseHandler() {
            @Override
            public void onResponse(RegisterResponse registerResponse) {
                removeProgress();
                if(registerResponse != null){
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            messageDialog.dismiss();
                        }
                    }).setMessage(registerResponse.getMessage())
                            .setTitle(registerResponse.getStatus());

                    if(registerResponse.getData() != null){
                        builder.setMessage(registerResponse.getMessage()+" \n "+registerResponse.getData());
                    }

                    messageDialog = builder.create();
                    messageDialog.show();


                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            messageDialog.dismiss();
                        }
                    }).setMessage("User Register Failed");

                    messageDialog = builder.create();
                    messageDialog.show();
                }
            }
        });
        request.execute();


    }



    public boolean isZipValid (String zip) {
        String zipRegex = "^[0-9]{5}$";
        Pattern zipPat = Pattern.compile(zipRegex);
        if(!zipPat.matcher(zip).matches()){return false;}
        else {return true;}
    }



}
