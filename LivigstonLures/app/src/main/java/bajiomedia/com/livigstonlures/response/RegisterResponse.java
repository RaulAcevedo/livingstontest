package bajiomedia.com.livigstonlures.response;

/**
 * Created by Raúl on 08/02/2016.
 */
public class RegisterResponse {

    private String status;
    private String message;
    private UserData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }
}
