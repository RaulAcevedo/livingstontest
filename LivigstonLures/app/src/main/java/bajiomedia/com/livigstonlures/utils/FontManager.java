package bajiomedia.com.livigstonlures.utils;

import android.content.res.AssetManager;
import android.graphics.Typeface;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Raúl on 07/02/2016.
 */
public class FontManager {

    private static FontManager instance;
    private AssetManager mgr;
    private Map<String, Typeface> fonts;

    private FontManager(AssetManager _mgr) {
        mgr = _mgr;
        fonts = new HashMap<>();
    }

    private FontManager() {
        fonts = new HashMap<>();
    }


    public static void init(AssetManager mgr) {
        if(instance == null) {
            instance = new FontManager(mgr);
        }else{
            instance.mgr = mgr;
        }
    }

    public static FontManager getInstance() {
        if(instance == null){
            instance = new FontManager();
        }
        return instance;
    }

    public Typeface getFont(String asset) {
        if (fonts.containsKey(asset))
            return fonts.get(asset);

        Typeface font = null;

        try {
            font = Typeface.createFromAsset(mgr, asset);
            fonts.put(asset, font);
        } catch (Exception e) {

        }

        if (font == null) {
            try {
                String fixedAsset = fixAssetFilename(asset);
                font = Typeface.createFromAsset(mgr, fixedAsset);
                fonts.put(asset, font);
                fonts.put(fixedAsset, font);
            } catch (Exception e) {

            }
        }

        return font;
    }

    private String fixAssetFilename(String asset) {
        // Empty font filename?
        // Just return it. We can't help.
        if (StringUtils.isEmpty(asset))
            return asset;

        // Make sure that the font ends in '.ttf' or '.ttc'
        if ((!asset.endsWith(".ttf")) && (!asset.endsWith(".ttc")))
            asset = String.format("%s.ttf", asset);

        return asset;
    }
}

