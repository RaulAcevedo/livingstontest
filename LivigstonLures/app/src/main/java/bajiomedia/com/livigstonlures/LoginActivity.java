package bajiomedia.com.livigstonlures;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.util.Locale;

import bajiomedia.com.livigstonlures.request.RegisterRequest;
import bajiomedia.com.livigstonlures.response.RegisterResponse;
import bajiomedia.com.livigstonlures.utils.FontManager;

public class LoginActivity extends BaseActivity {

    private EditText emailInput,passwordInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        prepareToolbar();

        emailInput = (EditText)findViewById(R.id.emailInput);
        passwordInput = (EditText)findViewById(R.id.passwordInput);
        Typeface tf = FontManager.getInstance().getFont("fonts/Roboto-Regular.ttf");
        emailInput.setTypeface(tf);
        passwordInput.setTypeface(tf);
    }

    public void onRegisterClick(View pTarget){
        Intent intent = new Intent(this,RegisterActivity.class);
        startActivity(intent);
        finish();

    }

    public void onForgetPasswordClick(View pTarget){
        Toast.makeText(this,"Forgot Password not Available",Toast.LENGTH_SHORT).show();
    }

    public void onLoginClick(View pTarget){

        String  email, password;
        email = emailInput.getText().toString().trim();
        password = passwordInput.getText().toString().trim();

        if(!StringUtils.isAnyEmpty( email, password))
        {
            if(!isEmailValid(email)) {
                Toast.makeText(LoginActivity.this, "Email Invalid", Toast.LENGTH_SHORT).show();
                return;
            }
            if(!isPasswordValid(password)) {
                Toast.makeText(LoginActivity.this, "Password Invalid", Toast.LENGTH_SHORT).show();
                return;
            }

            try {
                password = AeSimpleSHA1.SHA1(PWD_SALT + password);
                JSONObject params = new JSONObject();
                params.accumulate("op","userLogin");
                params.accumulate("password",password);
                params.accumulate("email",email);
                doLogin(params);
            }catch(Exception e){
                Toast.makeText(LoginActivity.this, "Failed to encrypt Password", Toast.LENGTH_SHORT).show();
            }

        }else{
            Toast.makeText(LoginActivity.this, "Please fill all the data", Toast.LENGTH_SHORT).show();
        }
    }


    private void doLogin(JSONObject paramObject){
        RegisterRequest request = new RegisterRequest();
        request.setParameters(paramObject);

        setProgressDialog(ProgressDialog.show(this, null, "Login In User...", true));
        request.setHandler(new RegisterRequest.ResponseHandler() {
            @Override
            public void onResponse(RegisterResponse registerResponse) {
                removeProgress();
                if(registerResponse != null){
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            messageDialog.dismiss();
                        }
                    }).setMessage(registerResponse.getMessage())
                            .setTitle(registerResponse.getStatus());

                    if(registerResponse.getData() != null){
                        builder.setMessage(registerResponse.getMessage()+" \n "+registerResponse.getData());
                    }

                    messageDialog = builder.create();
                    messageDialog.show();


                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            messageDialog.dismiss();
                        }
                    }).setMessage("User Login Failed");

                    messageDialog = builder.create();
                    messageDialog.show();
                }
            }
        });
        request.execute();


    }
}
