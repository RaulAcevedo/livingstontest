package bajiomedia.com.livigstonlures.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

import org.apache.commons.lang3.StringUtils;

import bajiomedia.com.livigstonlures.R;
import bajiomedia.com.livigstonlures.utils.FontManager;

/**
 * Created by Raúl on 07/02/2016.
 */
public class FontButton extends Button {

    public FontButton(Context context) {
        this(context, null);
    }

    public FontButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
    }

    public FontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        FontManager.init(context.getAssets());
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.FontTextView);
        if (ta != null) {
            String fontAsset = ta.getString(R.styleable.FontTextView_typefaceAsset);

            if (!StringUtils.isEmpty(fontAsset)) {
                Typeface tf = FontManager.getInstance().getFont(fontAsset);
                int style = Typeface.NORMAL;
                float size = getTextSize();

                if (getTypeface() != null)
                    style = getTypeface().getStyle();

                if (tf != null)
                    setTypeface(tf, style);
                else
                    Log.d("FontText", String.format("Could not create a font from asset: %s", fontAsset));
            }
        }
    }
}
