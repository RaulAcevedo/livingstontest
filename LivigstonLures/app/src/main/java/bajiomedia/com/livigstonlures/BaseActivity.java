package bajiomedia.com.livigstonlures;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

/**
 * Created by Raúl on 07/02/2016.
 */
public class BaseActivity extends AppCompatActivity {

    protected static final String PWD_SALT = "qJB0rGtIn5UB1xG03efyCp";

    private Toolbar screenToolbar;
    private TextView titleText;
    protected ProgressDialog progressDialog;
    protected Dialog messageDialog;

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public  void removeProgress(){
        if(progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.enteranim, R.anim.leaveanim);
    }

    protected void prepareToolbar(){
        screenToolbar = (Toolbar)findViewById(R.id.header_toolbar);
        titleText = (TextView)findViewById(R.id.title_text);
        if(screenToolbar != null){
            setSupportActionBar(screenToolbar);
        }
        try{
            PackageManager pm = getPackageManager();
            ActivityInfo activityInfo = pm.getActivityInfo (getComponentName(), 0);
            titleText.setText(activityInfo.loadLabel (pm).toString ());
        }catch(PackageManager.NameNotFoundException exception){

        }

        TextView toolbarTitle = null;
        for (int i = 0; i < screenToolbar.getChildCount(); ++i) {
            View child = screenToolbar.getChildAt(i);

            // assuming that the title is the first instance of TextView
            // you can also check if the title string matches
            if (child instanceof TextView && child != titleText) {
                toolbarTitle = (TextView) child;
                break;
            }
        }
        toolbarTitle.setLayoutParams(new android.support.v7.widget.Toolbar.LayoutParams(
                Toolbar.LayoutParams.MATCH_PARENT,
                Toolbar.LayoutParams.WRAP_CONTENT));
        toolbarTitle.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enterbackanim, R.anim.leavebackanim);
    }


    public boolean isEmailValid (String email) {
        //Regular Expression for a valid Email
        String emailRegex = "[\\w|.|-]*@\\w*\\.[\\w|.]*";

        Pattern emailPat = Pattern.compile(emailRegex);
        if(!emailPat.matcher(email).matches()){return false;}
        else {return true;}
    }

    public boolean isPasswordValid (String password) {
        //Regular Expression for a valid Email
        String upperCase = "[A-Z]+";
        String lowerCase = "[a-z]+";

        Pattern upperPat = Pattern.compile(upperCase);
        Pattern lowerPat = Pattern.compile(lowerCase);

        if(!upperPat.matcher(password).find()){return false;}
        if(!lowerPat.matcher(password).find()){return false;}
        if(password.length() < 8){return false;}
        return true;
    }

    static class AeSimpleSHA1 {
        private static String convertToHex(byte[] data) {
            StringBuilder buf = new StringBuilder();
            for (byte b : data) {
                int halfbyte = (b >>> 4) & 0x0F;
                int two_halfs = 0;
                do {
                    buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                    halfbyte = b & 0x0F;
                } while (two_halfs++ < 1);
            }
            return buf.toString();
        }

        public static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(text.getBytes("iso-8859-1"), 0, text.length());
            byte[] sha1hash = md.digest();
            return convertToHex(sha1hash);
        }
    }


}
