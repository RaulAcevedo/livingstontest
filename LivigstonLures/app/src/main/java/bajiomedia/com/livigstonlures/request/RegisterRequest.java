package bajiomedia.com.livigstonlures.request;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import bajiomedia.com.livigstonlures.response.RegisterResponse;

/**
 * Created by Raúl on 08/02/2016.
 */
public class RegisterRequest extends AsyncTask<Void,Integer,RegisterResponse> {

    private JSONObject parameters;
    private ResponseHandler handler;


    public ResponseHandler getHandler() {
        return handler;
    }

    public void setHandler(ResponseHandler handler) {
        this.handler = handler;
    }

    protected String getServiceURL(){
        return "http://api.livingstonlures.com/app.php";
    }


    public JSONObject getParameters() {
        return parameters;
    }

    public void setParameters(JSONObject parameters) {
        this.parameters = parameters;
    }


    @Override
    protected RegisterResponse doInBackground(Void... params) {
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(getServiceURL());

            String json = "";
            json = parameters.toString();

            StringEntity se = new StringEntity(json);
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");


            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();


            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

            Gson gson = new GsonBuilder().create();
            JsonObject object = (new JsonParser()).parse(result).getAsJsonObject();
            RegisterResponse response = gson.fromJson(object,RegisterResponse.class);
            return response;

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return null;
    }

    @Override
    protected void onPostExecute(RegisterResponse registerResponse) {
        super.onPostExecute(registerResponse);
        if(handler != null){
            handler.onResponse(registerResponse);
        }
    }

    private  String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public interface ResponseHandler{
        void onResponse(RegisterResponse registerResponse);
    }
}
